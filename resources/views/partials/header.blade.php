{{--
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ route('product.index')}}">eShop</a>
    </div> 

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
     
      <ul class="nav navbar-nav navbar-right">
        <li>
        <a href="{{ route('product.shoppingCart')}}"><i class="fa fa-shopping-cart"></i> Shopping Cart
        <span class="badge">{{ Session::has('cart') ? Session::get('cart')->totalQty: '' }}</span>
        </a>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> User Management <span class="caret"></span></a>
          <ul class="dropdown-menu">
          @if(Auth::check())
          <li><a href="{{ route('user.profile') }}">User Profile</a></li>
           <li role="separator" class="divider"></li>
            <li><a href="{{ route('user.logout') }}">Logout</a></li>
          @else
          <li><a href="{{ route('user.signup') }}">SignUp / Register</a></li>

            <li><a href="{{ route('user.signin') }}">SignIn</a></li>
          
          @endif
              
           
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>--}}


<div id="header">
  <div class="header3">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-12">
          <div class="logo3">
            <a href="index.html"><img src="images/home3/logo.png" alt="" /></a>
          </div>
        </div>
        <div class="col-md-6 col-sm-5 col-xs-12">
          <div class="smart-search search-form3">
            <div class="select-category">
              <a href="#" class="category-toggle-link">All</a>
              <ul class="list-category-toggle sub-menu-top">
                <li><a href="#">Computer &amp; Office</a></li>
                <li><a href="#">Elextronics</a></li>
                <li><a href="#">Jewelry &amp; Watches</a></li>
                <li><a href="#">Home &amp; Garden</a></li>
                <li><a href="#">Bags &amp; Shoes</a></li>
                <li><a href="#">Kids &amp; Baby</a></li>
              </ul>
            </div>
            <form class="smart-search-form">
              <input type="text"  name="search" value="i’m shopping for..." onfocus="if (this.value==this.defaultValue) this.value = ''" onblur="if (this.value=='') this.value = this.defaultValue" />
              <input type="submit" value="" />
            </form>
          </div>
        </div>
        <div class="col-md-3 col-sm-4 col-xs-12">
          <div class="wrap-cart-info3">
            <ul class="top-info top-info3">
              <li class="top-account has-child">
                <a href="#"><i class="fa fa-user"></i></a>
                <ul class="sub-menu-top">
                  <li><a href="#"><i class="fa fa-user"></i> Account Info</a></li>
                  <li><a href="#"><i class="fa fa-heart-o"></i> Wish List</a></li>
                  <li><a href="#"><i class="fa fa-toggle-on"></i> Compare</a></li>
                  <li><a href="#"><i class="fa fa-unlock-alt"></i> Sign in</a></li>
                  <li><a href="#"><i class="fa fa-sign-in"></i> Checkout</a></li>
                </ul>
              </li>
              <li class="top-language has-child">
                <a href="#" class="language-selected"><img src="images/home3/flag-en.png" alt=""/></a>
                <ul class="sub-menu-top">
                  <li><a href="#"><img src="images/home1/flag-england.jpg" alt=""/>English</a></li>
                  <li><a href="#"><img src="images/home1/flag-french.jpg" alt=""/>French</a></li>
                  <li><a href="#"><img src="images/home1/flag-german.jpg" alt=""/>German</a></li>
                </ul>
              </li>
              <li class="top-currency has-child">
                <a href="#" class="currency-selected">$</a>
                <ul class="sub-menu-top">
                  <li><a href="#"><span>€</span>EUR</a></li>
                  <li><a href="#"><span>¥</span>JPY</a></li>
                  <li><a href="#"><span>$</span>USD</a></li>
                </ul>
              </li>
            </ul>
            <div class="mini-cart mini-cart-3">
              <a  href="cart.html" class="header-mini-cart3">
                <span class="total-mini-cart-icon"></span>
                <span class="total-mini-cart-item">0</span>
              </a>
              <div class="content-mini-cart">
                <h2>(2) ITEMS IN MY CART</h2>
                <ul class="list-mini-cart-item">
                  <li>
                    <div class="mini-cart-edit">
                      <a class="delete-mini-cart-item" href="#"><i class="fa fa-trash-o"></i></a>
                      <a class="edit-mini-cart-item" href="#"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="mini-cart-thumb">
                      <a href="#"><img alt="" src="images/home1/mini-cart-thumb.png"></a>
                    </div>
                    <div class="mini-cart-info">
                      <h3><a href="#">Burberry Pink &amp; black</a></h3>
                      <div class="info-price">
                        <span>$59.52</span>
                        <del>$17.96</del>
                      </div>
                      <div class="qty-product">
                        <span class="qty-down">-</span>
                        <span class="qty-num">1</span>
                        <span class="qty-up">+</span>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="mini-cart-edit">
                      <a class="delete-mini-cart-item" href="#"><i class="fa fa-trash-o"></i></a>
                      <a class="edit-mini-cart-item" href="#"><i class="fa fa-pencil"></i></a>
                    </div>
                    <div class="mini-cart-thumb">
                      <a href="#"><img alt="" src="images/home1/mini-cart-thumb.png"></a>
                    </div>
                    <div class="mini-cart-info">
                      <h3><a href="#">Burberry Pink &amp; black</a></h3>
                      <div class="info-price">
                        <span>$59.52</span>
                        <del>$17.96</del>
                      </div>
                      <div class="qty-product">
                        <span class="qty-down">-</span>
                        <span class="qty-num">1</span>
                        <span class="qty-up">+</span>
                      </div>
                    </div>
                  </li>
                </ul>
                <div class="mini-cart-total">
                  <label>TOTAL</label>
                  <span>$24.28</span>
                </div>
                <div class="mini-cart-button">
                  <a class="mini-cart-view" href="#">view my cart </a>
                  <a class="mini-cart-checkout" href="#">Checkout</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- End Header 3 -->
  <div class="header-nav3">
    <div class="container">
      <nav class="main-nav main-nav3">
        <ul>
          <li class="menu-item">
            <a href="#">home</a>

          </li>
          <li class="has-mega-menu">
            <a href="grid.html">Fashion</a>
            <div class="mega-menu mega-menu-style1">
              <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="mega-hot-deal">
                    <h2 class="mega-menu-title">Hot deals</h2>
                    <div class="mega-hot-deal-slider">
                      <div class="wrap-item">
                        <div class="item-deal-product">
                          <div class="product-thumb">
                            <a href="#" class="product-thumb-link">
                              <img src="images/photos/furniture/6.jpg" alt="" class="first-thumb">
                              <img src="images/photos/furniture/5.jpg" alt="" class="second-thumb">
                            </a>
                            <div class="product-info-cart">
                              <div class="product-extra-link">
                                <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                              </div>
                              <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3 class="title-product"><a href="#">Pok Chair Classicle</a></h3>
                            <p class="desc">Lorem Khaled Ipsum is a major key to suc cess. Another one. </p>
                            <div class="info-price-deal">
                              <span>$59.52</span> <label>-30%</label>
                            </div>
                            <div class="deal-shop-social">
                              <a class="deal-shop-link" href="#">shop now</a>
                              <div class="social-deal social-network">
                                <ul>
                                  <li><a href="#"><img src="images/home1/s1.png" alt=""></a></li>
                                  <li><a href="#"><img src="images/home1/s2.png" alt=""></a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Item -->
                        <div class="item-deal-product">
                          <div class="product-thumb">
                            <a href="#" class="product-thumb-link">
                              <img src="images/photos/extras/17.jpg" alt="" class="first-thumb">
                              <img src="images/photos/extras/16.jpg" alt="" class="second-thumb">
                            </a>
                            <div class="product-info-cart">
                              <div class="product-extra-link">
                                <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                              </div>
                              <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3 class="title-product"><a href="#">Fashion Mangto</a></h3>
                            <p class="desc">Lorem Khaled Ipsum is a major key to suc cess. Another one. </p>
                            <div class="info-price-deal">
                              <span>$59.52</span> <label>-30%</label>
                            </div>
                            <div class="deal-shop-social">
                              <a class="deal-shop-link" href="#">shop now</a>
                              <div class="social-deal social-network">
                                <ul>
                                  <li><a href="#"><img src="images/home1/s1.png" alt=""></a></li>
                                  <li><a href="#"><img src="images/home1/s2.png" alt=""></a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Item -->
                        <div class="item-deal-product">
                          <div class="product-thumb">
                            <a href="#" class="product-thumb-link">
                              <img src="images/photos/sport/7.jpg" alt="" class="first-thumb">
                              <img src="images/photos/sport/6.jpg" alt="" class="second-thumb">
                            </a>
                            <div class="product-info-cart">
                              <div class="product-extra-link">
                                <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                              </div>
                              <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3 class="title-product"><a href="#">T-Shirt Sport</a></h3>
                            <p class="desc">Lorem Khaled Ipsum is a major key to suc cess. Another one. </p>
                            <div class="info-price-deal">
                              <span>$59.52</span> <label>-30%</label>
                            </div>
                            <div class="deal-shop-social">
                              <a class="deal-shop-link" href="#">shop now</a>
                              <div class="social-deal social-network">
                                <ul>
                                  <li><a href="#"><img src="images/home1/s1.png" alt=""></a></li>
                                  <li><a href="#"><img src="images/home1/s2.png" alt=""></a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Item -->
                        <div class="item-deal-product">
                          <div class="product-thumb">
                            <a href="#" class="product-thumb-link">
                              <img src="images/photos/extras/14.jpg" alt="" class="first-thumb">
                              <img src="images/photos/extras/13.jpg" alt="" class="second-thumb">
                            </a>
                            <div class="product-info-cart">
                              <div class="product-extra-link">
                                <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                              </div>
                              <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
                            </div>
                          </div>
                          <div class="product-info">
                            <h3 class="title-product"><a href="#">Bag Goodscol model</a></h3>
                            <p class="desc">Lorem Khaled Ipsum is a major key to suc cess. Another one. </p>
                            <div class="info-price-deal">
                              <span>$59.52</span> <label>-30%</label>
                            </div>
                            <div class="deal-shop-social">
                              <a class="deal-shop-link" href="#">shop now</a>
                              <div class="social-deal social-network">
                                <ul>
                                  <li><a href="#"><img src="images/home1/s1.png" alt=""></a></li>
                                  <li><a href="#"><img src="images/home1/s2.png" alt=""></a></li>
                                </ul>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Item -->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                  <div class="mega-new-arrival">
                    <h2 class="mega-menu-title">New Arrivals</h2>
                    <div class="mega-new-arrival-slider">
                      <div class="wrap-item">
                        <div class="item">
                          <div class="item-product">
                            <div class="product-thumb">
                              <a href="detail.html" class="product-thumb-link">
                                <img src="images/photos/extras/18.jpg" alt="" class="first-thumb">
                                <img src="images/photos/extras/17.jpg" alt="" class="second-thumb">
                              </a>
                              <div class="product-info-cart">
                                <div class="product-extra-link">
                                  <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                  <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                  <a href="quick-view.html" class="quickview-link fancybox.ajax"><i class="fa fa-search"></i></a>
                                </div>
                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h3 class="title-product"><a href="#">Burberry Pink &amp; black</a></h3>
                              <div class="info-price">
                                <span>$59.52</span><del>$17.96</del>
                              </div>
                              <div class="product-rating">
                                <div style="width:100%" class="inner-rating"></div>
                                <span>(6s)</span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Item -->
                        <div class="item">
                          <div class="item-product">
                            <div class="product-thumb">
                              <a href="detail.html" class="product-thumb-link">
                                <img src="images/photos/extras/21.jpg" alt="" class="first-thumb">
                                <img src="images/photos/extras/20.jpg" alt="" class="second-thumb">
                              </a>
                              <div class="product-info-cart">
                                <div class="product-extra-link">
                                  <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                  <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                  <a href="quick-view.html" class="quickview-link fancybox.ajax"><i class="fa fa-search"></i></a>
                                </div>
                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h3 class="title-product"><a href="#">Burberry Pink &amp; black</a></h3>
                              <div class="info-price">
                                <span>$59.52</span><del>$17.96</del>
                              </div>
                              <div class="product-rating">
                                <div style="width:100%" class="inner-rating"></div>
                                <span>(6s)</span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Item -->
                        <div class="item">
                          <div class="item-product">
                            <div class="product-thumb">
                              <a href="detail.html" class="product-thumb-link">
                                <img src="images/photos/extras/19.jpg" alt="" class="first-thumb">
                                <img src="images/photos/extras/15.jpg" alt="" class="second-thumb">
                              </a>
                              <div class="product-info-cart">
                                <div class="product-extra-link">
                                  <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                  <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                  <a href="quick-view.html" class="quickview-link fancybox.ajax"><i class="fa fa-search"></i></a>
                                </div>
                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h3 class="title-product"><a href="#">Burberry Pink &amp; black</a></h3>
                              <div class="info-price">
                                <span>$59.52</span><del>$17.96</del>
                              </div>
                              <div class="product-rating">
                                <div style="width:100%" class="inner-rating"></div>
                                <span>(6s)</span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Item -->
                        <div class="item">
                          <div class="item-product">
                            <div class="product-thumb">
                              <a href="detail.html" class="product-thumb-link">
                                <img src="images/photos/extras/3.jpg" alt="" class="first-thumb">
                                <img src="images/photos/extras/4.jpg" alt="" class="second-thumb">
                              </a>
                              <div class="product-info-cart">
                                <div class="product-extra-link">
                                  <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                  <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                  <a href="quick-view.html" class="quickview-link fancybox.ajax"><i class="fa fa-search"></i></a>
                                </div>
                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h3 class="title-product"><a href="#">Burberry Pink &amp; black</a></h3>
                              <div class="info-price">
                                <span>$59.52</span><del>$17.96</del>
                              </div>
                              <div class="product-rating">
                                <div style="width:100%" class="inner-rating"></div>
                                <span>(6s)</span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Item -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="has-mega-menu">
            <a href="list.html">Furniture</a>
            <div class="mega-menu">
              <div class="row">
                <div class="col-md-5 col-sm-5 col-xs-12">
                  <div class="mega-adv">
                    <div class="mega-adv-thumb zoom-image-thumb">
                      <a href="#"><img src="images/photos/newintoday/bag-shoes.jpg" alt="" /></a>
                    </div>
                    <div class="mega-adv-info">
                      <h3><a href="#">Examplui coloniu tencaug</a></h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                      <a class="more-detail" href="#">More Detail</a>
                    </div>
                  </div>
                </div>
                <div class="col-md-7 col-sm-7 col-xs-12">
                  <div class="mega-new-arrival">
                    <h2 class="mega-menu-title">Featured Product</h2>
                    <div class="mega-new-arrival-slider">
                      <div class="wrap-item">
                        <div class="item">
                          <div class="item-product">
                            <div class="product-thumb">
                              <a href="detail.html" class="product-thumb-link">
                                <img src="images/photos/extras/17.jpg" alt="" class="first-thumb">
                                <img src="images/photos/extras/18.jpg" alt="" class="second-thumb">
                              </a>
                              <div class="product-info-cart">
                                <div class="product-extra-link">
                                  <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                  <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                  <a href="quick-view.html" class="quickview-link fancybox.ajax"><i class="fa fa-search"></i></a>
                                </div>
                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h3 class="title-product"><a href="#">Burberry Pink &amp; black</a></h3>
                              <div class="info-price">
                                <span>$59.52</span><del>$17.96</del>
                              </div>
                              <div class="product-rating">
                                <div style="width:100%" class="inner-rating"></div>
                                <span>(6s)</span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Item -->
                        <div class="item">
                          <div class="item-product">
                            <div class="product-thumb">
                              <a href="detail.html" class="product-thumb-link">
                                <img src="images/photos/extras/20.jpg" alt="" class="first-thumb">
                                <img src="images/photos/extras/21.jpg" alt="" class="second-thumb">
                              </a>
                              <div class="product-info-cart">
                                <div class="product-extra-link">
                                  <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                  <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                  <a href="quick-view.html" class="quickview-link fancybox.ajax"><i class="fa fa-search"></i></a>
                                </div>
                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h3 class="title-product"><a href="#">Burberry Pink &amp; black</a></h3>
                              <div class="info-price">
                                <span>$59.52</span><del>$17.96</del>
                              </div>
                              <div class="product-rating">
                                <div style="width:100%" class="inner-rating"></div>
                                <span>(6s)</span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Item -->
                        <div class="item">
                          <div class="item-product">
                            <div class="product-thumb">
                              <a href="detail.html" class="product-thumb-link">
                                <img src="images/photos/extras/15.jpg" alt="" class="first-thumb">
                                <img src="images/photos/extras/19.jpg" alt="" class="second-thumb">
                              </a>
                              <div class="product-info-cart">
                                <div class="product-extra-link">
                                  <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                  <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                  <a href="quick-view.html" class="quickview-link fancybox.ajax"><i class="fa fa-search"></i></a>
                                </div>
                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h3 class="title-product"><a href="#">Burberry Pink &amp; black</a></h3>
                              <div class="info-price">
                                <span>$59.52</span><del>$17.96</del>
                              </div>
                              <div class="product-rating">
                                <div style="width:100%" class="inner-rating"></div>
                                <span>(6s)</span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Item -->
                        <div class="item">
                          <div class="item-product">
                            <div class="product-thumb">
                              <a href="detail.html" class="product-thumb-link">
                                <img src="images/photos/extras/4.jpg" alt="" class="first-thumb">
                                <img src="images/photos/extras/3.jpg" alt="" class="second-thumb">
                              </a>
                              <div class="product-info-cart">
                                <div class="product-extra-link">
                                  <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                  <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                  <a href="quick-view.html" class="quickview-link fancybox.ajax"><i class="fa fa-search"></i></a>
                                </div>
                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i> Add to Cart</a>
                              </div>
                            </div>
                            <div class="product-info">
                              <h3 class="title-product"><a href="#">Burberry Pink &amp; black</a></h3>
                              <div class="info-price">
                                <span>$59.52</span><del>$17.96</del>
                              </div>
                              <div class="product-rating">
                                <div style="width:100%" class="inner-rating"></div>
                                <span>(6s)</span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- End Item -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
          <li class="menu-item-has-children">
            <a href="grid.html">Food</a>
            <ul class="sub-menu">
              <li><a href="#">Pizza</a></li>
              <li><a href="#">Noodle</a></li>
              <li class="menu-item-has-children">
                <a href="#">Cake</a>
                <ul class="sub-menu">
                  <li><a href="#">lemon cake</a></li>
                  <li><a href="#">mousse cake</a></li>
                  <li><a href="#">carrot cake</a></li>
                  <li><a href="#">chocolate cake</a></li>
                </ul>
              </li>
              <li><a href="#">Drink</a></li>
            </ul>
          </li>
          <li class="menu-item-has-children">
            <a href="grid.html">Electronis</a>
            <ul class="sub-menu">
              <li><a href="#">Mobile</a></li>
              <li><a href="#">Laptop</a></li>
              <li><a href="#">Camera</a></li>
              <li><a href="#">Accessories</a></li>
            </ul>
          </li>
          <li><a href="list.html">Sports</a></li>
          <li class="menu-item">
            <a href="blog-v1.html">Blog</a>
          </li>
        </ul>
        <a href="#" class="toggle-mobile-menu"><span>Menu</span></a>
      </nav>
      <!-- End Main Nav -->
    </div>
  </div>
  <!-- End Main Nav -->
</div>

