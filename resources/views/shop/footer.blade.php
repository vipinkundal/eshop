<div id="footer">
    <div class="list-gift-icon">
        <div class="container">
            <div class="gift-icon-slider">
                <div class="wrap-item">
                    <div class="item">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-ms-12">
                                <div class="item-gift-icon">
                                    <span><i class="fa fa-gift"></i></span>
                                    <h2>gift Voucher</h2>
                                    <p>Lorem ipsum dolor sit amet conse ctetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-ms-12">
                                <div class="item-gift-icon">
                                    <span><i class="fa fa-twitch"></i></span>
                                    <h2>Testimonials</h2>
                                    <p>“Lorem ipsum dolor sit amet conse ctetur adip isicing elit, sed do eiusmod tempor incididunt abore et dolore magna aliqua.”</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-ms-12">
                                <div class="item-gift-icon">
                                    <span><i class="fa fa-shopping-basket"></i></span>
                                    <h2>Guide order</h2>
                                    <p>Lorem ipsum dolor sit amet conse ctetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-ms-12">
                                <div class="item-gift-icon">
                                    <span><i class="fa fa-gift"></i></span>
                                    <h2>gift Voucher</h2>
                                    <p>Lorem ipsum dolor sit amet conse ctetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-ms-12">
                                <div class="item-gift-icon">
                                    <span><i class="fa fa-twitch"></i></span>
                                    <h2>Testimonials</h2>
                                    <p>“Lorem ipsum dolor sit amet conse ctetur adip isicing elit, sed do eiusmod tempor incididunt abore et dolore magna aliqua.”</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-ms-12">
                                <div class="item-gift-icon">
                                    <span><i class="fa fa-shopping-basket"></i></span>
                                    <h2>Guide order</h2>
                                    <p>Lorem ipsum dolor sit amet conse ctetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-ms-12">
                                <div class="item-gift-icon">
                                    <span><i class="fa fa-gift"></i></span>
                                    <h2>gift Voucher</h2>
                                    <p>Lorem ipsum dolor sit amet conse ctetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-ms-12">
                                <div class="item-gift-icon">
                                    <span><i class="fa fa-twitch"></i></span>
                                    <h2>Testimonials</h2>
                                    <p>“Lorem ipsum dolor sit amet conse ctetur adip isicing elit, sed do eiusmod tempor incididunt abore et dolore magna aliqua.”</p>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-ms-12">
                                <div class="item-gift-icon">
                                    <span><i class="fa fa-shopping-basket"></i></span>
                                    <h2>Guide order</h2>
                                    <p>Lorem ipsum dolor sit amet conse ctetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End List Gift -->
    <div class="footer3">
        <div class="container">
            <div class="list-menu-footer3">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="box-menu-footer3">
                            <h2>HELP</h2>
                            <ul>
                                <li><a href="#">Payments</a></li>
                                <li><a href="#">Saved Cards</a></li>
                                <li><a href="#">Shipping</a></li>
                                <li><a href="#">Cancellation & Returns</a></li>
                                <li><a href="#">FAQ</a></li>
                                <li><a href="#">Report Infringement</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="box-menu-footer3">
                            <h2>AloShop</h2>
                            <ul>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Specials</a></li>
                                <li><a href="#">New products</a></li>
                                <li><a href="#">Top sellers</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="box-menu-footer3">
                            <h2>MY ACCOUNT</h2>
                            <ul>
                                <li><a href="#">My Order</a></li>
                                <li><a href="#">My Wishlist</a></li>
                                <li><a href="#">My Credit Slip</a></li>
                                <li><a href="#">My Addresses</a></li>
                                <li><a href="#">My Personal In</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="box-menu-footer3">
                            <h2>Customer</h2>
                            <ul>
                                <li><a href="#">Online Shopping</a></li>
                                <li><a href="#">Affiliate Program</a></li>
                                <li><a href="#">Gift Card</a></li>
                                <li><a href="#">AloShop First Subscription</a></li>
                                <li><a href="#">AloShop First Subscription</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End List Menu -->
            <div class="social-footer-box3">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="newsletter-footer newsletter-footer3">
                            <label>newsletter</label>
                            <form>
                                <input type="text" onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="Enter Your E-mail">
                                <input type="submit" value="">
                            </form>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="social-footer social-footer3 social-network">
                            <label>KEEP IN TOUCH</label>
                            <ul>
                                <li><a href="#"><img alt="" src="images/home1/s1.png"></a></li>
                                <li><a href="#"><img alt="" src="images/home1/s2.png"></a></li>
                                <li><a href="#"><img alt="" src="images/home1/s3.png"></a></li>
                                <li><a href="#"><img alt="" src="images/home1/s4.png"></a></li>
                                <li><a href="#"><img alt="" src="images/home1/s5.png"></a></li>
                                <li><a href="#"><img alt="" src="images/home1/s6.png"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Social Footer 3 -->
        </div>
    </div>
    <!-- End Footer 3 -->
    <div class="footer-bottom3">
        <div class="container">
            <div class="social-copyright3">
                <div class="row">
                    <div class="col-md-8 col-sm-8 col-xs-12">
                        <p class="copyright3">©2016 AloShop</p>
                        <div class="policy3">
                            <label>Policies: </label>
                            <a href="#">Terms of use</a>
                            <a href="#">Security</a>
                            <a href="#">Privacy</a>
                            <a href="#">Infringement</a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="findin-store">
                            <a href="#"><i class="fa fa-map-marker"></i> <span>find store</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Social -->
        </div>
    </div>
</div>
