@extends('layouts.master')

@section('title')
	eShop - Home
@endsection

@section('content')
        {{--@foreach($index_products->chunk(3) as $chunk)
            <div class="row">
            @foreach ($chunk as $product)
          <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
              <img src="{{ $product->imagePath }}" alt="eshop-images" class="img-responsive">
              <div class="caption">
                <h3>{{ $product->title }}</h3>
                <p class="description"> {{ $product->description }}</p>
                <div class="clearfix">
                <div class="pull-left price">${{ $product->price }}</div>
                    <a href="{{ route('product.addToCart', ['id' => $product->id]) }}" class="btn btn-success pull-right" role="button">Add to Cart</a>
                </div>
              </div>
            </div>
            </div>
            @endforeach
          </div>
        @endforeach--}}


        <div id="content">
            <div class="content-home3">
                <div class="container">
                    <div class="quick-category">
                        <a href="grid.html"><img src="images/home3/icon1.png" alt="" /> Mobiles & Tablets</a>
                        <a href="grid.html"><img src="images/home3/icon2.png" alt="" /> Computers</a>
                        <a href="grid.html"><img src="images/home3/icon3.png" alt="" /> Electronics</a>
                        <a href="grid.html"><img src="images/home3/icon4.png" alt="" /> Fashion</a>
                        <a href="grid.html"><img src="images/home3/icon5.png" alt="" /> Jewelry & Watches</a>
                        <a href="grid.html"><img src="images/home3/icon6.png" alt="" /> Home & Kitchen</a>
                    </div>
                    <!-- End Quick Category -->
                    <div class="box-top-home3">
                        <div class="row">
                            <div class="col-md-9 col-sm-8 col-xs-12">
                                <div class="thenew-banner">
                                    <div class="thenew-slider slider-home3">
                                        <div class="wrap-item">
                                            <div class="item-thenew">
                                                <div class="thenew-thumb">
                                                    <a href="#"><img src="images/home3/slide1.png" alt="" /></a>
                                                </div>
                                                <div class="thenew-info">
                                                    <h2>The New Galaxy S7</h2>
                                                    <a href="#">DISCOVER NOW</a>
                                                </div>
                                            </div>
                                            <!-- End Item -->
                                            <div class="item-thenew">
                                                <div class="thenew-thumb">
                                                    <a href="#"><img src="images/home3/slide2.png" alt="" /></a>
                                                </div>
                                                <div class="thenew-info">
                                                    <h2>Omega Model Watch</h2>
                                                    <a href="#">DISCOVER NOW</a>
                                                </div>
                                            </div>
                                            <!-- End Item -->
                                            <div class="item-thenew">
                                                <div class="thenew-thumb">
                                                    <a href="#"><img src="images/home3/slide3.png" alt="" /></a>
                                                </div>
                                                <div class="thenew-info">
                                                    <h2>Oppo Smart Phone</h2>
                                                    <a href="#">DISCOVER NOW</a>
                                                </div>
                                            </div>
                                            <!-- End Item -->
                                            <div class="item-thenew">
                                                <div class="thenew-thumb">
                                                    <a href="#"><img src="images/home3/slide4.png" alt="" /></a>
                                                </div>
                                                <div class="thenew-info">
                                                    <h2>Laptop Dell XPS L51</h2>
                                                    <a href="#">DISCOVER NOW</a>
                                                </div>
                                            </div>
                                            <!-- End Item -->
                                        </div>
                                    </div>
                                    <!-- End New Slider -->
                                </div>
                                <!-- End New Banner -->
                                <div class="service3">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="item-service3">
                                                <div class="item-service-thum3">
                                                    <a href="#"><img src="images/home3/around1.png" alt="" /></a>
                                                </div>
                                                <div class="item-service-info3">
                                                    <h2><a href="#">It's simpler</a></h2>
                                                    <p>Lorem Khaled Ipsum </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="item-service3">
                                                <div class="item-service-thum3">
                                                    <a href="#"><img src="images/home3/around2.png" alt="" /></a>
                                                </div>
                                                <div class="item-service-info3">
                                                    <h2><a href="#">It's safer</a></h2>
                                                    <p>Lorem Khaled Ipsum </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4 col-xs-12">
                                            <div class="item-service3">
                                                <div class="item-service-thum3">
                                                    <a href="#"><img src="images/home3/around3.png" alt="" /></a>
                                                </div>
                                                <div class="item-service-info3">
                                                    <h2><a href="#">It's faster</a></h2>
                                                    <p>Lorem Khaled Ipsum </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Service -->
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <div class="box-trending3">
                                    <h2><span>trendding</span></h2>
                                    <ul class="list-trending3">
                                        <li class="item-trending3">
                                            <div class="trending-thumb3 product-thumb">
                                                <a href="#"><img src="images/photos/extras/3.jpg" alt="" /></a>
                                                <a class="addcart-link addcart-single" href="#"><i class="fa fa-shopping-basket"></i></a>
                                            </div>
                                            <div class="product-info3">
                                                <h3 class="title-product"><a href="#">Model Watch</a></h3>
                                                <p class="desc">industry's standa dummy tever </p>
                                                <div class="info-price">
                                                    <span>$249.00</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="item-trending3">
                                            <div class="trending-thumb3 product-thumb">
                                                <a href="#"><img src="images/photos/furniture/27.jpg" alt="" /></a>
                                                <a class="addcart-link addcart-single" href="#"><i class="fa fa-shopping-basket"></i></a>
                                            </div>
                                            <div class="product-info3">
                                                <h3 class="title-product"><a href="#">Light Room</a></h3>
                                                <p class="desc">Printer took a galley of type </p>
                                                <div class="info-price">
                                                    <span>$310.00</span>
                                                    <del>$341.00</del>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="item-trending3">
                                            <div class="trending-thumb3 product-thumb">
                                                <a href="#"><img src="images/photos/fashion/2.png" alt="" /></a>
                                                <a class="addcart-link addcart-single" href="#"><i class="fa fa-shopping-basket"></i></a>
                                            </div>
                                            <div class="product-info3">
                                                <h3 class="title-product"><a href="#">Bag Fashion</a></h3>
                                                <p class="desc">Make a type specimen book</p>
                                                <div class="info-price">
                                                    <span>$480.00</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- End Trending -->
                            </div>
                        </div>
                    </div>
                    <!-- End Box Top -->
                    <div class="box-best-seller3">
                        <div class="row">
                            <div class="col-md-9 col-sm-8 col-xs-12">
                                <div class="best-seller3 slider-home3">
                                    <h2><span>best sellers</span></h2>
                                    <div class="wrap-item">
                                        <div class="item">
                                            <div class="item-product3">
                                                <div class="product-thumb product-thumb3">
                                                    <a href="#" class="product-thumb-link">
                                                        <img class="first-thumb" src="images/photos/fashion/4.png" alt=""/>
                                                        <img class="second-thumb" src="images/photos/fashion/2.png" alt=""/>
                                                    </a>
                                                    <div class="product-info-cart">
                                                        <div class="product-extra-link">
                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                    </div>
                                                </div>
                                                <div class="product-info3">
                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                    <div class="info-price">
                                                        <span>$40.60 </span>
                                                    </div>
                                                    <div class="product-rating">
                                                        <div class="inner-rating" style="width:100%"></div>
                                                        <span>(1s)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Item -->
                                        <div class="item">
                                            <div class="item-product3">
                                                <div class="product-thumb product-thumb3">
                                                    <a href="#" class="product-thumb-link">
                                                        <img class="first-thumb" src="images/photos/extras/17.jpg" alt=""/>
                                                        <img class="second-thumb" src="images/photos/extras/16.jpg" alt=""/>
                                                    </a>
                                                    <div class="product-info-cart">
                                                        <div class="product-extra-link">
                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                    </div>
                                                </div>
                                                <div class="product-info3">
                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                    <div class="info-price">
                                                        <span>$30.99 </span>
                                                        <del>$327.00</del>
                                                    </div>
                                                    <div class="product-rating">
                                                        <div class="inner-rating" style="width:100%"></div>
                                                        <span>(1s)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Item -->
                                        <div class="item">
                                            <div class="item-product3">
                                                <div class="product-thumb product-thumb3">
                                                    <a href="#" class="product-thumb-link">
                                                        <img class="first-thumb" src="images/photos/fashion/14.png" alt=""/>
                                                        <img class="second-thumb" src="images/photos/fashion/13.png" alt=""/>
                                                    </a>
                                                    <div class="product-info-cart">
                                                        <div class="product-extra-link">
                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                    </div>
                                                </div>
                                                <div class="product-info3">
                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                    <div class="info-price">
                                                        <span>$59.52</span>
                                                    </div>
                                                    <div class="product-rating">
                                                        <div class="inner-rating" style="width:100%"></div>
                                                        <span>(1s)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Item -->
                                        <div class="item">
                                            <div class="item-product3">
                                                <div class="product-thumb product-thumb3">
                                                    <a href="#" class="product-thumb-link">
                                                        <img class="first-thumb" src="images/photos/fashion/5.png" alt=""/>
                                                        <img class="second-thumb" src="images/photos/fashion/3.png" alt=""/>
                                                    </a>
                                                    <div class="product-info-cart">
                                                        <div class="product-extra-link">
                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                    </div>
                                                </div>
                                                <div class="product-info3">
                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                    <div class="info-price">
                                                        <span>$87.00 </span>
                                                        <del>$200.00</del>
                                                    </div>
                                                    <div class="product-rating">
                                                        <div class="inner-rating" style="width:100%"></div>
                                                        <span>(1s)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Item -->
                                        <div class="item">
                                            <div class="item-product3">
                                                <div class="product-thumb product-thumb3">
                                                    <a href="#" class="product-thumb-link">
                                                        <img class="first-thumb" src="images/photos/fashion/12.png" alt=""/>
                                                        <img class="second-thumb" src="images/photos/fashion/11.png" alt=""/>
                                                    </a>
                                                    <div class="product-info-cart">
                                                        <div class="product-extra-link">
                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                    </div>
                                                </div>
                                                <div class="product-info3">
                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                    <div class="info-price">
                                                        <span>$87.00 </span>
                                                    </div>
                                                    <div class="product-rating">
                                                        <div class="inner-rating" style="width:100%"></div>
                                                        <span>(1s)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Item -->
                                        <div class="item">
                                            <div class="item-product3">
                                                <div class="product-thumb product-thumb3">
                                                    <a href="#" class="product-thumb-link">
                                                        <img class="first-thumb" src="images/photos/fashion/8.png" alt=""/>
                                                        <img class="second-thumb" src="images/photos/fashion/7.png" alt=""/>
                                                    </a>
                                                    <div class="product-info-cart">
                                                        <div class="product-extra-link">
                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                    </div>
                                                </div>
                                                <div class="product-info3">
                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                    <div class="info-price">
                                                        <span>$87.00 </span>
                                                        <del>$200.00</del>
                                                    </div>
                                                    <div class="product-rating">
                                                        <div class="inner-rating" style="width:100%"></div>
                                                        <span>(1s)</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Item -->
                                    </div>
                                    <a href="#" class="viewall3">View All</a>
                                </div>
                                <!-- End Best Seller -->
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-12">
                                <div class="ad-best-seller zoom-image-thumb">
                                    <a href="#"><img src="images/home3/ad1.png" alt="" /></a>
                                </div>
                                <div class="ad-best-seller zoom-image-thumb">
                                    <a href="#"><img src="images/home3/ad11.png" alt="" /></a>
                                </div>
                                <div class="ad-best-seller zoom-image-thumb">
                                    <a href="#"><img src="images/home3/ad3.png" alt="" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Box Best Seller -->
                    <div class="dealoff-theday">
                        <h2><span>Deals of the Day</span></h2>
                        <div class="dealoff-countdown" data-date="12/22/2016"></div>
                        <div class="dealoff-theday-slider slider-home3">
                            <div class="wrap-item">
                                <div class="item-dealoff">
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/beauty/6.jpg" alt=""/>
                                                <img class="second-thumb" src="images/photos/fashion/5.png" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                            <div class="info-price">
                                                <span>$79.00</span>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/fashion/2.png" alt=""/>
                                                <img class="second-thumb" src="images/photos/fashion/1.png" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">Hugo puches lipen</a></h3>
                                            <div class="info-price">
                                                <span>$20.00</span>
                                                <del>$40.00</del>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Item -->
                                <div class="item-dealoff">
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/food/7.jpg" alt=""/>
                                                <img class="second-thumb" src="images/photos/food/8.jpg" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">Phormat bocuoi</a></h3>
                                            <div class="info-price">
                                                <span>$68.00</span>
                                                <del>$70.00</del>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/furniture/11.jpg" alt=""/>
                                                <img class="second-thumb" src="images/photos/furniture/10.jpg" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">Phormat bocuoi</a></h3>
                                            <div class="info-price">
                                                <span>$32.00</span>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Item -->
                                <div class="item-dealoff">
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/sport/1.jpg" alt=""/>
                                                <img class="second-thumb" src="images/photos/sport/6.jpg" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">Adidad fashion</a></h3>
                                            <div class="info-price">
                                                <span>$130.99</span>
                                                <del>$150.00</del>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/furniture/7.jpg" alt=""/>
                                                <img class="second-thumb" src="images/photos/furniture/5.jpg" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">Adidad fashion</a></h3>
                                            <div class="info-price">
                                                <span>$250.00</span>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Item -->
                                <div class="item-dealoff">
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/sport/7.jpg" alt=""/>
                                                <img class="second-thumb" src="images/photos/sport/7.jpg" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">nile fashion</a></h3>
                                            <div class="info-price">
                                                <span>$80.00 </span>
                                                <del>$90.00</del>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/fashion/6.png" alt=""/>
                                                <img class="second-thumb" src="images/photos/fashion/9.png" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">nile fashion</a></h3>
                                            <div class="info-price">
                                                <span>$280.00</span>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Item -->
                                <div class="item-dealoff">
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/beauty/7.jpg" alt=""/>
                                                <img class="second-thumb" src="images/photos/beauty/6.jpg" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                            <div class="info-price">
                                                <span>$93.75</span>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/sport/18.jpg" alt=""/>
                                                <img class="second-thumb" src="images/photos/sport/17.jpg" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                            <div class="info-price">
                                                <span>$93.00 </span>
                                                <del>$100.00 </del>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Item -->
                                <div class="item-dealoff">
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/sport/23.jpg" alt=""/>
                                                <img class="second-thumb" src="images/photos/sport/24.jpg" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">Adidad fashion</a></h3>
                                            <div class="info-price">
                                                <span>$130.99</span>
                                                <del>$150.00</del>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/furniture/23.jpg" alt=""/>
                                                <img class="second-thumb" src="images/photos/furniture/21.jpg" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">Adidad fashion</a></h3>
                                            <div class="info-price">
                                                <span>$250.00</span>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Item -->
                                <div class="item-dealoff">
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/beauty/4.jpg" alt=""/>
                                                <img class="second-thumb" src="images/photos/beauty/5.jpg" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                            <div class="info-price">
                                                <span>$93.75</span>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-product">
                                        <div class="product-thumb product-thumb3">
                                            <a href="#" class="product-thumb-link">
                                                <img class="first-thumb" src="images/photos/sport/23.jpg" alt=""/>
                                                <img class="second-thumb" src="images/photos/sport/24.jpg" alt=""/>
                                            </a>
                                            <div class="product-info-cart">
                                                <div class="product-extra-link">
                                                    <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                    <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                    <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                </div>
                                                <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                            </div>
                                        </div>
                                        <div class="product-info3">
                                            <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                            <div class="info-price">
                                                <span>$93.00 </span>
                                                <del>$100.00 </del>
                                            </div>
                                            <div class="product-rating">
                                                <div class="inner-rating" style="width:100%"></div>
                                                <span>(1s)</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Item -->
                            </div>
                        </div>
                    </div>
                    <!-- End Deal -->
                    <div class="mobile-access">
                        <h2 class="title-home3">mobile accessories</h2>
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="mobile-access-box mobile-access-long">
                                            <div class="mobile-access-thumb">
                                                <a href="#"><img src="images/home3/ac1.png" alt="" /></a>
                                            </div>
                                            <div class="product-info3">
                                                <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                <div class="info-price">
                                                    <span>$480.00</span>
                                                    <del>$591.00</del>
                                                </div>
                                                <a href="#" class="shopnow-access">shop now</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="mobile-access-box mobile-access-text">
                                            <h2>Accessories</h2>
                                            <div class="access-list-link clearfix">
                                                <ul>
                                                    <li><a href="#">Headphone</a></li>
                                                    <li><a href="#">Sharp</a></li>
                                                    <li><a href="#">Bumper</a></li>
                                                    <li><a href="#">Easycover</a></li>
                                                </ul>
                                                <ul>
                                                    <li><a href="#">Sticker</a></li>
                                                    <li><a href="#">screen</a></li>
                                                    <li><a href="#">USB cable</a></li>
                                                </ul>
                                            </div>
                                            <a href="#" class="access-more">More <i class="fa fa-chevron-circle-right"></i></a>
                                        </div>
                                        <div class="mobile-access-box">
                                            <div class="mobile-access-thumb">
                                                <a href="#"><img src="images/home3/ac2.png" alt="" /></a>
                                            </div>
                                            <div class="product-info3">
                                                <h3 class="title-product"><a href="#">lioa lamol </a></h3>
                                                <div class="info-price">
                                                    <span>$200.00 </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="mobile-access-box mobile-access-col2 clearfix">
                                            <div class="mobile-access-thumb">
                                                <a href="#"><img src="images/home3/ac3.png" alt="" /></a>
                                            </div>
                                            <div class="product-info3">
                                                <h3 class="title-product"><a href="#">rom color os viettel</a></h3>
                                                <div class="info-price">
                                                    <span>$90.00 </span>
                                                    <del>$120.00</del>
                                                </div>
                                                <a href="#" class="shopnow-access">shop now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="mobile-access-box mobile-access-col2 clearfix">
                                            <div class="mobile-access-thumb">
                                                <a href="#"><img src="images/home3/ac4.png" alt="" /></a>
                                            </div>
                                            <div class="product-info3">
                                                <h3 class="title-product"><a href="#">sacpin iLike 7800mah</a></h3>
                                                <div class="info-price">
                                                    <span>$100.00 </span>
                                                    <del>$150.00</del>
                                                </div>
                                                <a href="#" class="shopnow-access">shop now</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="mobile-access-box  mobile-access-long">
                                            <div class="mobile-access-thumb">
                                                <a href="#"><img src="images/home3/ac5.png" alt="" /></a>
                                            </div>
                                            <div class="product-info3">
                                                <h3 class="title-product"><a href="#">sac pin NPK2018</a></h3>
                                                <div class="info-price">
                                                    <span>$480.00</span>
                                                    <del>$591.00</del>
                                                </div>
                                                <a href="#" class="shopnow-access">shop now</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="mobile-access-box">
                                            <div class="mobile-access-thumb">
                                                <a href="#"><img src="images/home3/ac6.png" alt="" /></a>
                                            </div>
                                            <div class="product-info3">
                                                <h3 class="title-product"><a href="#">Capda trinon</a></h3>
                                                <div class="info-price">
                                                    <span>$90.00 </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mobile-access-box">
                                            <div class="mobile-access-thumb">
                                                <a href="#"><img src="images/home3/ac7.png" alt="" /></a>
                                            </div>
                                            <div class="product-info3">
                                                <h3 class="title-product"><a href="#">capites onclik</a></h3>
                                                <div class="info-price">
                                                    <span>$508.00</span>
                                                    <del>$590.00</del>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Mobile Access -->
                    <div class="popular-cat">
                        <h2 class="title-home3">popular categories</h2>
                        <div class="popular-cat-box">
                            <div class="row">
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="popular-cat-sidebar">
                                        <h2>fashion</h2>
                                        <ul class="popular-listcat">
                                            <li><a href="#">Street Style</a></li>
                                            <li><a href="#">Designer</a></li>
                                            <li><a href="#">Dresses</a></li>
                                            <li><a href="#">Accessories</a></li>
                                        </ul>
                                        <div class="category-brand-slider">
                                            <div class="wrap-item">
                                                <div class="item">
                                                    <div class="item-category-brand">
                                                        <a href="#"><img src="images/home1/pn12.png" alt="" /></a>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-category-brand">
                                                        <a href="#"><img src="images/home1/pn2.png" alt="" /></a>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-category-brand">
                                                        <a href="#"><img src="images/home1/pn3.png" alt="" /></a>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-category-brand">
                                                        <a href="#"><img src="images/home1/pn6.png" alt="" /></a>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-category-brand">
                                                        <a href="#"><img src="images/home1/pn5.png" alt="" /></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Category Brand Slider -->
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-8 col-xs-12">
                                    <div class="popular-cat-content">
                                        <div class="popular-cat-tab-title">
                                            <ul>
                                                <li class="active"><a href="#best1" data-toggle="tab">best sellers</a></li>
                                                <li><a href="#new1" data-toggle="tab">new products</a></li>
                                            </ul>
                                            <a href="#" class="viewall">View all</a>
                                        </div>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="best1">
                                                <div class="popular-cat-slider slider-home3">
                                                    <div class="wrap-item">
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/fashion/4.png" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/fashion/2.png" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$40.60 </span>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/extras/17.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/extras/16.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$30.99 </span>
                                                                        <del>$327.00</del>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/fashion/14.png" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/fashion/13.png" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$59.52</span>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/fashion/5.png" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/fashion/3.png" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$87.00 </span>
                                                                        <del>$200.00</del>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/fashion/12.png" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/fashion/11.png" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$87.00 </span>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/fashion/8.png" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/fashion/7.png" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$87.00 </span>
                                                                        <del>$200.00</del>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="new1">
                                                <div class="popular-cat-slider slider-home3">
                                                    <div class="wrap-item">
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/food/11.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/food/10.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$40.60 </span>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/food/4.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/food/3.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$30.99 </span>
                                                                        <del>$327.00</del>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/food/2.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/food/1.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$59.52</span>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/food/12.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/food/5.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$87.00 </span>
                                                                        <del>$200.00</del>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/food/7.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/food/6.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$87.00 </span>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/food/8.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/food/9.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$87.00 </span>
                                                                        <del>$200.00</del>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Box -->
                        <div class="popular-cat-box yellow-box">
                            <div class="row">
                                <div class="col-md-9 col-sm-8 col-xs-12">
                                    <div class="popular-cat-content">
                                        <div class="popular-cat-tab-title">
                                            <ul>
                                                <li class="active"><a href="#best2" data-toggle="tab">best sellers</a></li>
                                                <li><a href="#new2" data-toggle="tab">new products</a></li>
                                            </ul>
                                            <a href="#" class="viewall">View all</a>
                                        </div>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active" id="best2">
                                                <div class="popular-cat-slider slider-home3">
                                                    <div class="wrap-item">
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/food/11.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/food/10.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$40.60 </span>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/food/4.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/food/3.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$30.99 </span>
                                                                        <del>$327.00</del>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/food/2.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/food/1.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$59.52</span>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/food/12.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/food/5.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$87.00 </span>
                                                                        <del>$200.00</del>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/food/7.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/food/6.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$87.00 </span>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/food/8.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/food/9.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$87.00 </span>
                                                                        <del>$200.00</del>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="new2">
                                                <div class="popular-cat-slider slider-home3">
                                                    <div class="wrap-item">
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/fashion/4.png" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/fashion/2.png" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$40.60 </span>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/extras/17.jpg" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/extras/16.jpg" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$30.99 </span>
                                                                        <del>$327.00</del>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/fashion/14.png" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/fashion/13.png" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$59.52</span>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/fashion/5.png" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/fashion/3.png" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$87.00 </span>
                                                                        <del>$200.00</del>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/fashion/12.png" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/fashion/11.png" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$87.00 </span>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                        <div class="item">
                                                            <div class="item-product3">
                                                                <div class="product-thumb product-thumb3">
                                                                    <a href="#" class="product-thumb-link">
                                                                        <img class="first-thumb" src="images/photos/fashion/8.png" alt=""/>
                                                                        <img class="second-thumb" src="images/photos/fashion/7.png" alt=""/>
                                                                    </a>
                                                                    <div class="product-info-cart">
                                                                        <div class="product-extra-link">
                                                                            <a href="#" class="wishlist-link"><i class="fa fa-heart-o"></i></a>
                                                                            <a href="#" class="compare-link"><i class="fa fa-toggle-on"></i></a>
                                                                            <a href="#" class="quickview-link"><i class="fa fa-search"></i></a>
                                                                        </div>
                                                                        <a href="#" class="addcart-link"><i class="fa fa-shopping-basket"></i>  Add to Cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-info3">
                                                                    <h3 class="title-product"><a href="#">Women's Woolen </a></h3>
                                                                    <div class="info-price">
                                                                        <span>$87.00 </span>
                                                                        <del>$200.00</del>
                                                                    </div>
                                                                    <div class="product-rating">
                                                                        <div class="inner-rating" style="width:100%"></div>
                                                                        <span>(1s)</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End Item -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-4 col-xs-12">
                                    <div class="popular-cat-sidebar">
                                        <h2>food</h2>
                                        <ul class="popular-listcat">
                                            <li><a href="#">Pizza Hut</a></li>
                                            <li><a href="#">Cake Kinhdo</a></li>
                                            <li><a href="#">Drink & Cafe</a></li>
                                            <li><a href="#">Pitest Tike</a></li>
                                        </ul>
                                        <div class="category-brand-slider">
                                            <div class="wrap-item">
                                                <div class="item">
                                                    <div class="item-category-brand">
                                                        <a href="#"><img src="images/home1/pn4.png" alt="" /></a>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-category-brand">
                                                        <a href="#"><img src="images/home1/pn2.png" alt="" /></a>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-category-brand">
                                                        <a href="#"><img src="images/home1/pn3.png" alt="" /></a>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-category-brand">
                                                        <a href="#"><img src="images/home1/pn6.png" alt="" /></a>
                                                    </div>
                                                </div>
                                                <div class="item">
                                                    <div class="item-category-brand">
                                                        <a href="#"><img src="images/home1/pn5.png" alt="" /></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Category Brand Slider -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Box -->
                    </div>
                    <!-- End Popular -->
                    <div class="adv-sale3">
                        <div class="item-adv-simple">
                            <a href="#"><img alt="" src="images/home3/ad2.png"></a>
                        </div>
                    </div>
                    <!-- End Adv -->
                    <div class="worldof-aloshop">
                        <h2 class="title-home3">world of aloshop</h2>
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <a href="#" class="world-ad-box">
                                    <img src="images/home3/alo1.png" alt="" />
                                    <div class="alo-smask"></div>
                                </a>
                                <a href="#" class="world-ad-box">
                                    <img src="images/home3/alo3.png" alt="" />
                                    <div class="alo-smask"></div>
                                </a>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <a href="#" class="world-ad-box">
                                    <img src="images/home3/alo2.png" alt="" />
                                    <div class="alo-smask"></div>
                                </a>
                                <a href="#" class="world-ad-box">
                                    <img src="images/home3/alo4.png" alt="" />
                                    <div class="alo-smask"></div>
                                </a>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="latest-testimo-tab">
                                    <div class="latest-testimo-title popular-cat-tab-title">
                                        <ul class="title-tab3">
                                            <li class="active"><a href="#latest" data-toggle="tab">latest news</a></li>
                                            <li><a href="#testimo" data-toggle="tab">testimonials</a></li>
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade in active" id="latest">
                                            <ul class="list-latest-new">
                                                <li class="clearfix">
                                                    <div class="zoom-image-thumb">
                                                        <a href="#"><img src="images/home3/news1.png" alt="" /></a>
                                                    </div>
                                                    <div class="latest-post-info">
                                                        <h3><a href="#">Our Company is committed  client saction</a></h3>
                                                        <ul class="comment-date-info">
                                                            <li><i class="fa fa-calendar-o"></i> 15 March 2016</li>
                                                            <li><i class="fa fa-comment-o"></i> 8</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="clearfix">
                                                    <div class="zoom-image-thumb">
                                                        <a href="#"><img src="images/home3/news2.png" alt="" /></a>
                                                    </div>
                                                    <div class="latest-post-info">
                                                        <h3><a href="#">Our Company is committed  client saction</a></h3>
                                                        <ul class="comment-date-info">
                                                            <li><i class="fa fa-calendar-o"></i> 15 March 2016</li>
                                                            <li><i class="fa fa-comment-o"></i> 8</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li class="clearfix">
                                                    <div class="zoom-image-thumb">
                                                        <a href="#"><img src="images/home3/news3.png" alt="" /></a>
                                                    </div>
                                                    <div class="latest-post-info">
                                                        <h3><a href="#">Our Company is committed  client saction</a></h3>
                                                        <ul class="comment-date-info">
                                                            <li><i class="fa fa-calendar-o"></i> 15 March 2016</li>
                                                            <li><i class="fa fa-comment-o"></i> 8</li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="testimo">
                                            <div class="tab-testimo-slider">
                                                <div class="wrap-item">
                                                    <div class="item">
                                                        <div class="item-testimo3">
                                                            <ul class="testimo-tab-info">
                                                                <li>
                                                                    <div class="zoom-image-thumb">
                                                                        <a href="#"><img src="images/home3/test1.png" alt="" /></a>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="author-test-info">
                                                                        <h3><a href="#">Janet Cummings</a></h3>
                                                                        <span>Beamsoft</span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <p class="desc">Proin urna enim, semper at egestas sed, elem entum in justo. Mauris sed mauris biben dum est imperdiet porttitor tincidunt at lorem. </p>
                                                        </div>
                                                        <div class="item-testimo3">
                                                            <ul class="testimo-tab-info">
                                                                <li>
                                                                    <div class="zoom-image-thumb">
                                                                        <a href="#"><img src="images/home3/test2.png" alt="" /></a>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="author-test-info">
                                                                        <h3><a href="#">Anadi Shikha Lanum</a></h3>
                                                                        <span>Dessign</span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <p class="desc">Proin urna enim, semper at egestas sed, elem entum in justo. Mauris sed mauris biben dum est imperdiet porttitor tincidunt at lorem. </p>
                                                        </div>
                                                    </div>
                                                    <!-- End Item -->
                                                    <div class="item">
                                                        <div class="item-testimo3">
                                                            <ul class="testimo-tab-info">
                                                                <li>
                                                                    <div class="zoom-image-thumb">
                                                                        <a href="#"><img src="images/home3/test2.png" alt="" /></a>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="author-test-info">
                                                                        <h3><a href="#">Janet Cummings</a></h3>
                                                                        <span>Beamsoft</span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <p class="desc">Proin urna enim, semper at egestas sed, elem entum in justo. Mauris sed mauris biben dum est imperdiet porttitor tincidunt at lorem. </p>
                                                        </div>
                                                        <div class="item-testimo3">
                                                            <ul class="testimo-tab-info">
                                                                <li>
                                                                    <div class="zoom-image-thumb">
                                                                        <a href="#"><img src="images/home3/test3.png" alt="" /></a>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="author-test-info">
                                                                        <h3><a href="#">Anadi Shikha Lanum</a></h3>
                                                                        <span>Dessign</span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <p class="desc">Proin urna enim, semper at egestas sed, elem entum in justo. Mauris sed mauris biben dum est imperdiet porttitor tincidunt at lorem. </p>
                                                        </div>
                                                    </div>
                                                    <!-- End Item -->
                                                    <div class="item">
                                                        <div class="item-testimo3">
                                                            <ul class="testimo-tab-info">
                                                                <li>
                                                                    <div class="zoom-image-thumb">
                                                                        <a href="#"><img src="images/home3/test3.png" alt="" /></a>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="author-test-info">
                                                                        <h3><a href="#">Janet Cummings</a></h3>
                                                                        <span>Beamsoft</span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <p class="desc">Proin urna enim, semper at egestas sed, elem entum in justo. Mauris sed mauris biben dum est imperdiet porttitor tincidunt at lorem. </p>
                                                        </div>
                                                        <div class="item-testimo3">
                                                            <ul class="testimo-tab-info">
                                                                <li>
                                                                    <div class="zoom-image-thumb">
                                                                        <a href="#"><img src="images/home3/test1.png" alt="" /></a>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="author-test-info">
                                                                        <h3><a href="#">Anadi Shikha Lanum</a></h3>
                                                                        <span>Dessign</span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <p class="desc">Proin urna enim, semper at egestas sed, elem entum in justo. Mauris sed mauris biben dum est imperdiet porttitor tincidunt at lorem. </p>
                                                        </div>
                                                    </div>
                                                    <!-- End Item -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End World of Aloshop -->
                    <div class="manufacturers-banner">
                        <h2 class="title-home3">Manufacturers</h2>
                        <div class="manufacture-slider">
                            <div class="wrap-item">
                                <div class="item-manufacture">
                                    <div class="zoom-image-thumb"><a href="#"><img src="images/home3/brand1.png" alt="" /></a></div>
                                </div>
                                <div class="item-manufacture">
                                    <div class="zoom-image-thumb"><a href="#"><img src="images/home3/brand2.png" alt="" /></a></div>
                                </div>
                                <div class="item-manufacture">
                                    <div class="zoom-image-thumb"><a href="#"><img src="images/home3/brand3.png" alt="" /></a></div>
                                </div>
                                <div class="item-manufacture">
                                    <div class="zoom-image-thumb"><a href="#"><img src="images/home3/brand4.png" alt="" /></a></div>
                                </div>
                                <div class="item-manufacture">
                                    <div class="zoom-image-thumb"><a href="#"><img src="images/home3/brand5.png" alt="" /></a></div>
                                </div>
                                <div class="item-manufacture">
                                    <div class="zoom-image-thumb"><a href="#"><img src="images/home3/brand6.png" alt="" /></a></div>
                                </div>
                                <div class="item-manufacture">
                                    <div class="zoom-image-thumb"><a href="#"><img src="images/home3/brand1.png" alt="" /></a></div>
                                </div>
                                <div class="item-manufacture">
                                    <div class="zoom-image-thumb"><a href="#"><img src="images/home3/brand2.png" alt="" /></a></div>
                                </div>
                                <div class="item-manufacture">
                                    <div class="zoom-image-thumb"><a href="#"><img src="images/home3/brand3.png" alt="" /></a></div>
                                </div>
                                <div class="item-manufacture">
                                    <div class="zoom-image-thumb"><a href="#"><img src="images/home3/brand4.png" alt="" /></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Manufacturers -->
                </div>
            </div>
        </div>




@endsection